$(document).ready(function(){
    $('body').append('<div id="cookie-directive"><div id="cookie-directive-header">'+cookie_directive_copy+'</div><div id="cookie-directive-footer"><a id="cookie-directive-yes">Yes</a><a id="cookie-directive-no">No</a></div>');
    var cd = $('#cookie-directive');
    var height = cd.outerHeight();    
    cd.css('bottom','-'+height+'px');    
    cd.animate({bottom: 0},1000);   
    
    $('#cookie-directive-footer a').click(function() {
        if($(this).attr('id') == 'cookie-directive-yes') {
            document.cookie = 'cookie-directive=1; expires=Mon, 1 Jan 2029 00:00:01 UTC; path=/';
        } else {
            var cookies = document.cookie.split(";");
            for (var i = 0; i < cookies.length; i++) document.cookie = cookies[i].split("=")[0]+"=null; expires=" + new Date(0).toUTCString();
            $('body').append('<div id="cookie-directive-fail">'+cookie_directive_fail+'</div>');
            $('#cookie-directive-fail a').click(function(e) {
                e.preventDefault();
                
                document.cookie = 'cookie-directive=1; expires=Mon, 1 Jan 2029 00:00:01 UTC; path=/';
                $('#cookie-directive-fail').fadeOut('slow');
                
                return false;
            });
        }
        cd.animate({bottom: '-'+height},1000);
    });
});

